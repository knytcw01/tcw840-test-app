package com.kny.knyDocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KnyDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KnyDockerApplication.class, args);
	}

}
