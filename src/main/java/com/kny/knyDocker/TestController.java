package com.kny.knyDocker;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	private static final String ok = "OK";

	@GetMapping("/test")
	public String test() {
		return ok;
	}
}